jQuery(document).ready(function () {

    jQuery(".popup").hide();

    let status = false;

    jQuery(".person-info").on("click", function (event) {

        let person = jQuery(this);

        if (status == false) {
            person.find(".items p:not(:first-child)").hide();
            person.find(".buttons h4:first-child").css("border-bottom", "3px solid #0F2463");
            person.find(".popup").fadeIn(300);
            status = true;
        } else {
            if (event.target.className == "popup") {
                person.find(".popup").fadeOut(300, function () {
                    person.find(".items p").show();
                    person.find(".buttons h4").css("border-bottom", "0");
                    status = false;
                });
            } else if (event.target.localName == "h4") {
                let id = jQuery(event.target).attr("id");
                person.find(".buttons h4").css("border-bottom", "0");
                jQuery(event.target).css("border-bottom", "3px solid #0F2463");
                person.find(".items p").hide();
                person.find("#text_" + id.split("_")[1]).fadeIn(300);
            }
        }
    })

});