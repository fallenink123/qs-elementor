jQuery(document).ready(function () {

    jQuery(".comment-respond").hide();

    jQuery("#btn_show_comment_form").on("click", function () {

        let form = jQuery(".comment-respond");

        form.fadeIn(300).css('display', 'flex');
    })

    jQuery(".comment-respond").on("click", function (event) {

        let form = jQuery(this);

        if (event.target.className == "comment-respond") {
            form.fadeOut(300, function () {
                form.css('display', 'none');
            })
        }

    })

});