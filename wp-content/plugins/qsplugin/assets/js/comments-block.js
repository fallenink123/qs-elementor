jQuery(document).ready(function () {
    const slider = jQuery("#comments-slider").owlCarousel({
        loop: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 250,
        responsive: {
            0: {
                items: 1
            },
            756: {
                items: 2
            },
            1150: {
                items: 3
            }
        }
    });
});