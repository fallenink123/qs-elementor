jQuery(document).ready(function () {

    let status = false;

    jQuery(".person-info").on("click", function (event) {

        let person = jQuery(this);

        if (status == false) {
            person.find(".items p:not(:first-child)").hide();
            person.find(".buttons h4:first-child").css("border-bottom", "3px solid #08a299");
            jQuery(".person-info .content .info").animate({ height: "toggle", padding: "0 0" }, 300);
            jQuery(".person-info").not(person).animate({ height: "toggle" }, function () {
                jQuery(person).animate({ width: "100%" }, 300);
                jQuery(person).find(".slider").animate({ width: "100%" }, 300).stop(true, true);
                status = true;
            });
        } else {
            if (event.target.localName != "h4") {
                jQuery(person).find(".slider").animate({ width: "0%" }, 300);
                jQuery(person).stop(true, true).animate({ width: "26%" }, 300, function () {
                    person.find(".items p").show();
                    person.find(".buttons h4").css("border-bottom", "0");
                    jQuery(".person-info").not(jQuery(person)).animate({ height: "toggle" });
                    jQuery(".person-info .content .info").animate({ height: "toggle", padding: "20px 15px" }, 300);
                    status = false;
                });
            } else {
                let id = jQuery(event.target).attr("id");
                person.find(".buttons h4").css("border-bottom", "0");
                jQuery(event.target).css("border-bottom", "3px solid #08a299");
                person.find(".items p").hide();
                person.find("#text_" + id.split("_")[1]).fadeIn(300);
            }
        }
    })

});