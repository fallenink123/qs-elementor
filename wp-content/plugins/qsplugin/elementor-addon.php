<?php
/**
 * Plugin Name: Team and Partners
 * Description: Widget for QualityStandart
 * Version:     1.0.0
 * Author:      Elementor Developer
 * Author URI:  https://developers.elementor.com/
 */


function register_team_and_partners_widget( $widgets_manager ) {

	require_once( __DIR__ . '/widgets/team-and-partners.php' );
	$widgets_manager->register( new \Team_And_Partners() );

}

function register_comments_form_widget( $widgets_manager ) {

	require_once( __DIR__ . '/widgets/comments-form.php' );
	$widgets_manager->register( new \Comments_Form() );

}

function register_comments_block_widget( $widgets_manager ) {

	require_once( __DIR__ . '/widgets/comments-block.php' );
	$widgets_manager->register( new \Comments_Block() );

}

add_action( 'elementor/widgets/register', 'register_team_and_partners_widget' );
add_action( 'elementor/widgets/register', 'register_comments_form_widget' );
add_action( 'elementor/widgets/register', 'register_comments_block_widget' );