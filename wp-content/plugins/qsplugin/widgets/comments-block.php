<?php
class Comments_Block extends \Elementor\Widget_Base {

	public function get_script_depends() {
		return [ 'owl-carousel-js', 'comments-block' ];
	}

	public function get_style_depends() {
		return [ 'owl-carousel-css', 'owl-theme' ];
	}

	public function get_name() {
		return 'hello_world_widget_4';
	}

	public function get_title() {
		return esc_html__( 'Comments Block', 'elementor-addon' );
	}

	public function get_icon() {
		return 'eicon-code';
	}

	protected function register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'plugin-name' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->end_controls_section();

	}

	protected function render() {

		$settings = $this->get_settings_for_display();

		echo '<div class="owl-carousel comments-slider" id="comments-slider">';
			foreach (get_comments() as $comment) {
				echo '<div class="comment-block">
						<div class="comment">
							<p>'. $comment->comment_content .'</p>
						</div>
						<div class="author">
							<div class="photo">'. get_avatar($comment->comment_ID) .'</div>
							<div class="info">
								<h3>'. $comment->comment_author .'</h3>
							</div>
						</div>
					  </div>';
			}
			echo  '</div>';

			wp_register_style( 'owl-carousel-css', get_site_url() . '/wp-content/plugins/qsplugin/assets/css/owl.carousel.min.css', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
			wp_register_style( 'owl-theme', get_site_url() . '/wp-content/plugins/qsplugin/assets/css/owl.theme.default.min.css', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
			wp_register_script( 'owl-carousel-js', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/owl.carousel.min.js', [ 'elementor-frontend', 'jquery'], '1.0.0', true );
			wp_register_script( 'comments-block', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/comments-block.js', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
	}
}
