<?php
class Team_And_Partners extends \Elementor\Widget_Base {

	public function get_script_depends() {
		return [ 'team-and-partners-popup', 'team-and-partners-slider' ];
	}

	public function get_name() {
		return 'hello_world_widget_2';
	}

	public function get_title() {
		return esc_html__( 'Team and Partners', 'elementor-addon' );
	}

	public function get_icon() {
		return 'eicon-code';
	}

	protected function register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'plugin-name' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'content_toggle',
			[
				'label' => esc_html__( 'Попап / слайд', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Попап', 'your-plugin' ),
				'label_off' => esc_html__( 'Слайд', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'list_name', [
				'label' => esc_html__( 'Імʼя', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

        $repeater->add_control(
			'list_photo', [
				'label' => esc_html__( 'Фото', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'list_position', [
				'label' => esc_html__( 'Посада', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

        $repeater->add_control(
			'list_experience', [
				'label' => esc_html__( 'Професійний стаж', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

        $repeater->add_control(
			'list_education', [
				'label' => esc_html__( 'Освіта', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

        $repeater->add_control(
			'list_internship', [
				'label' => esc_html__( 'Стажування, підвищення кваліфікації', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'list',
			[
				'label' => esc_html__( 'Repeater List', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ list_name }}}',
			]
		);

		$this->end_controls_section();

	}

	public function get_description($settings, $item) {

		$btn_experience = (!empty($item["list_experience"])) ? '<h4 id="btn_experience">Професійний стаж</h4>' : '';
		$btn_education = (!empty($item["list_education"])) ? '<h4 id="btn_education">Освіта</h4>' : '';
		$btn_internship = (!empty($item["list_internship"])) ? '<h4 id="btn_internship">Підвищення кваліфікації</h4>' : '';
		
		if ($settings['content_toggle'] == "yes") {
			wp_register_script( 'team-and-partners-popup', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/team-and-partners-popup.js', [ 'elementor-frontend' ], '1.0.0', true );
			$description = '<div class="popup">
								<div class="content">
									<div class="left">
										<div style="background-image: url('. $item["list_photo"]["url"] .');"></div>
									</div>
									<div class="right">
										<div>
											<h3>'. $item["list_name"] .'</h3>
											<p>'. $item["list_position"] .'</p>
										</div>
										<div class="buttons">'
											. $btn_experience . $btn_education . $btn_internship .
										'</div>
										<div class="items">
											<p id="text_experience">'. $item["list_experience"] .'</p>
											<p id="text_education">'. $item["list_education"] .'</p>
											<p id="text_internship">'. $item["list_internship"] .'</p>
										</div>
									</div>
								</div>
							</div>';
		} else {
			wp_register_script( 'team-and-partners-slider', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/team-and-partners-slider.js', [ 'elementor-frontend' ], '1.0.0', true );
			$description = '<div class="slider">
								<div class="right">
									<div>
										<h3>'. $item["list_name"] .'</h3>
										<p>'. $item["list_position"] .'</p>
									</div>
									<div class="buttons">'
											. $btn_experience . $btn_education . $btn_internship .
									'</div>
									<div class="items">
										<p id="text_experience">'. $item["list_experience"] .'</p>
										<p id="text_education">'. $item["list_education"] .'</p>
										<p id="text_internship">'. $item["list_internship"] .'</p>
									</div>
								</div>
							</div>';
		}

		return $description;
	}

	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( $settings['list'] ) {
            echo '<div class="persons">';
                foreach ( $settings['list'] as $item ) {
                    echo '<div class="person-info">';
						echo '<div class="content">
								  <div class="photo" style="background-image: url('. $item["list_photo"]["url"] .');"></div>
								  <div class="info">
							 		  <h3>'. $item["list_name"] .'</h3>
									  <p>'. $item["list_position"] .'</p>
								  </div>
							  </div>';
							  echo $this->get_description($settings, $item);
                    echo '</div>';
                }
            echo '</div>';
		}
	}
}
