<?php
class Comments_Form extends \Elementor\Widget_Base {

	public function get_script_depends() {
		return [ 'comments-form' ];
	}

	public function get_name() {
		return 'hello_world_widget_3';
	}

	public function get_title() {
		return esc_html__( 'Comments Form', 'elementor-addon' );
	}

	public function get_icon() {
		return 'eicon-code';
	}

	protected function register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Поля', 'plugin-name' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'content_button', [
				'label' => esc_html__( 'Кнопка', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'content_name', [
				'label' => esc_html__( 'Інпут', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'content_comment', [
				'label' => esc_html__( 'Текстареа', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'content_submit', [
				'label' => esc_html__( 'Сабміт', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->end_controls_section();

	}

	protected function render() {

		$settings = $this->get_settings_for_display();

		wp_register_script( 'comments-form', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/comments-form.js', [ 'elementor-frontend' ], '1.0.0', true );

		echo '<div class="comments-form-button" id="btn_show_comment_form">' . $settings['content_button'] . '</div>
			  <div class="comments-form">
			  '. comment_form() .'
			  </div>';
	}
}
