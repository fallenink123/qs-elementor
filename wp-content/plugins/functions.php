<?php
/**
 * The template includes necessary functions for theme.
 *
 * @package karma
 * @since 1.0.0
 */


if (! function_exists('karma_child_scripts')) {
    function karma_child_scripts(){
    	
		 // register style
        wp_enqueue_style('karma-child-css', get_stylesheet_directory_uri() . '/style.css');

    	
    }
}
add_action( 'wp_enqueue_scripts', 'karma_child_scripts');
add_filter('https_ssl_verify', '__return_false');

function enable_svg_upload( $upload_mimes ) {
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'enable_svg_upload', 10, 1 );




add_filter( 'comment_form_defaults', 'set_my_comment_title' );

function set_my_comment_title( $defaults ) {
	$defaults['comment_field'] = '<textarea id="comment" name="comment" rows="4" required placeholder="Коментар"></textarea>';
	$defaults['fields']['author'] = '<input id="author" name="author" type="text" required placeholder="Імʼя">';
	$defaults['submit_button'] = '<input type="submit" value="Залишити коментар">';

	return $defaults;
}

// settings > discussion >unselectComment author must fill out name and email

add_filter('comment_form_default_fields', 'url_filtered');
function url_filtered($fields)
{
  if(isset($fields['url']))
   unset($fields['url']);
  return $fields;
}

add_filter('comment_form_default_fields', 'email_filtered');
function email_filtered($fields)
{
  if(isset($fields['email']))
   unset($fields['email']);
  return $fields;
}

wp_enqueue_style( 'owl-carousel-css', get_site_url() . '/wp-content/plugins/qsplugin/assets/css/owl.carousel.min.css', ['jquery'] );
wp_enqueue_style( 'owl-theme', get_site_url() . '/wp-content/plugins/qsplugin/assets/css/owl.theme.default.min.css', ['jquery'] );
wp_enqueue_script( 'owl-carousel-js', get_site_url() . '/wp-content/plugins/qsplugin/assets/js/owl.carousel.min.js', ['jquery', 'owl-carousel-css', 'owl-theme'] );
