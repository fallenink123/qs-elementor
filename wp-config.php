<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tests' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g$8;K,^zo)8#|E1YdSX$om}ufP];QHp{,S0b7_GYE}sZ5:& E=Kp79PufEG4~ N<' );
define( 'SECURE_AUTH_KEY',  '$cH(8l~jQDj0`g%ko{5`7z@w!%58I4rj4/UlOjMU0@ZfKcUd<FYfTN7F`CSH+~Yl' );
define( 'LOGGED_IN_KEY',    '~/`JR,V8>Q1]zu~N{7ZGT;7~Xq1B?xyC!MqG%~!I<oJ(?_./vh!ld@xVM8Lp$P]f' );
define( 'NONCE_KEY',        'Q&)2yn6`b]$?XbJ ]eA*AIUvK`$-_ngi{UC+qk38:r:E5QCiR5]/,]qOKtlF8Pt#' );
define( 'AUTH_SALT',        'h29&AVEX,G`JG(j|C@2.5<hj^XP*1%wzr/d+jAFyLphGfdiph7A S:lv!aH2XZL]' );
define( 'SECURE_AUTH_SALT', 'o+VeBd2Dmx8ozR?BLZ!&3 kt<&p2::rAt3,Kv3d]kFJsd^?VwQoAiNa[;6 WBd12' );
define( 'LOGGED_IN_SALT',   'Y?q<$|_/rMyLfhfdXL9917G$(&-7_laBfG(: j#NT?|OGT*Y$g}b;-3AyxD?xOA1' );
define( 'NONCE_SALT',       '}F&9-~9_0BF^d*4Dn(QAF4_8E?|qw.a;P4:%m~jpkU+}Twj 9$wf1Vi,F)p%v=F)' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define ('FS_METHOD', 'direct');